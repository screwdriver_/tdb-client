#ifndef TDB_CLIENT_HPP
# define TDB_CLIENT_HPP

# include<iostream>
# include<string>

# include<termios.h>

# include<tdb_driver.hpp>

# define VERSION		"0.1"

# define DEFAULT_PORT	8123

namespace TDb
{
	using namespace std;
	using namespace Network;

	bool quiet = false;

	inline void print(const string str)
	{
		if(quiet) return;
		cout << str;
	}
}

#endif
